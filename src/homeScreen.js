import React from "react";
import { View, Button } from "react-native";
import { useNavigation } from "@react-navigation/native";

const HomeScreen = () => {
  const navigation = useNavigation();
  const handleImage = () => {
    navigation.navigate("ImageMoviment");
    };
    const handleCount = () => {
        navigation.navigate("CountMoviment");
  };
  return (
    <View>
      <Button title="Ir para Aba Imagens" onPress={handleImage} />
      <Button title="Ir para Aba Count" onPress={handleCount} color="blue" />
    </View>
  );
};
export default HomeScreen;